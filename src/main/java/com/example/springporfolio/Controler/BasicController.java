package com.example.springporfolio.Controler;

import com.example.springporfolio.Modelo.*;
import com.example.springporfolio.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@Controller
public class BasicController {
    @Autowired
    private PersonalService personalService;
    @Autowired
    private LenguajesService lenguajesService;
    @Autowired
    private Lenguajes2Service lenguajes2Service;
    @Autowired
    private FotoperfilService fotoperfilService;
    @Autowired
    private ProyectosService proyectosService;
    @Autowired
    private ServicesService servicesService;
    @Autowired
    private ServicesService2 servicesService2;
    @Autowired
    private ServicesService3 servicesService3;
    @Autowired
    private ContactameService contactameService;

    @PostMapping("/")
    String add(@RequestParam String correo_contatame, @RequestParam String comentario_contactame, Model model){
        System.out.println("iniciando post");
        Contactame m = new Contactame();
        m.setCorreo_contatame(correo_contatame);
        m.setComentario_contactame(comentario_contactame);
        contactameService.addContactame(m);
        model.addAttribute("mensaje", "Se ha enviado el correo   " + correo_contatame );
        ArrayList<Personal> personals = personalService.getPersonal();
        model.addAttribute("personal", personals.get(2));

        // Devuelve el nombre de la vista que debe renderizarse
        ArrayList<Lenguajes> lenguajess = lenguajesService.getImg_lenguaje();
        model.addAttribute("lenguajes", lenguajess);

        ArrayList<Lenguajes2> lenguajess2 = lenguajes2Service.getImg_lenguaje();
        model.addAttribute("lenguajes2", lenguajess2);

        ArrayList<Fotosperfil> fotoperfils = fotoperfilService.getImg_Fotosperfil();
        model.addAttribute("fotosperfil", fotoperfils);

        ArrayList<Proyectos> proyectoss = proyectosService.getImg_proyecto();
        model.addAttribute("proyectos", proyectoss);

        ArrayList<Services> servicess = servicesService.getImg_service();
        model.addAttribute("services", servicess);

        ArrayList<Services2> servicess2 = servicesService2.getImg_service();
        model.addAttribute("services2", servicess2);

        ArrayList<Services3> servicess3 = servicesService3.getImg_service();
        model.addAttribute("services3", servicess3);
        return "Porfoliopaloma";
    }

    @GetMapping("/")
    public String Paginaweb( Model model) {
        System.out.println("iniciando get");
        ArrayList<Personal> personals = personalService.getPersonal();
        model.addAttribute("personal", personals.get(2));

        // Devuelve el nombre de la vista que debe renderizarse
        ArrayList<Lenguajes> lenguajess = lenguajesService.getImg_lenguaje();
        model.addAttribute("lenguajes", lenguajess);

        ArrayList<Lenguajes2> lenguajess2 = lenguajes2Service.getImg_lenguaje();
        model.addAttribute("lenguajes2", lenguajess2);

        ArrayList<Fotosperfil> fotoperfils = fotoperfilService.getImg_Fotosperfil();
        model.addAttribute("fotosperfil", fotoperfils);

        ArrayList<Proyectos> proyectoss = proyectosService.getImg_proyecto();
        model.addAttribute("proyectos", proyectoss);

        ArrayList<Services> servicess = servicesService.getImg_service();
        model.addAttribute("services", servicess);

        ArrayList<Services2> servicess2 = servicesService2.getImg_service();
        model.addAttribute("services2", servicess2);

        ArrayList<Services3> servicess3 = servicesService3.getImg_service();
        model.addAttribute("services3", servicess3);

        return "Porfoliopaloma";
}
}
