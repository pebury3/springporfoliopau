package com.example.springporfolio.repository;

import com.example.springporfolio.Modelo.Contactame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactameRepository extends JpaRepository<Contactame, Long> {
}
