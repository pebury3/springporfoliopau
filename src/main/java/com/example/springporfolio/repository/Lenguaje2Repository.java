package com.example.springporfolio.repository;

import com.example.springporfolio.Modelo.Lenguajes2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Lenguaje2Repository extends JpaRepository<Lenguajes2, Long> {
}
