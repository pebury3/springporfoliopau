package com.example.springporfolio.repository;

import com.example.springporfolio.Modelo.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalRepository extends JpaRepository <Personal, Long>{
}
