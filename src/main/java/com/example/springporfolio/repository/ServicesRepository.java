package com.example.springporfolio.repository;


import com.example.springporfolio.Modelo.Services;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicesRepository extends JpaRepository<Services, Long> {
}
