package com.example.springporfolio.repository;


import com.example.springporfolio.Modelo.Proyectos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProyectosRepository extends JpaRepository<Proyectos, Long> {
}

