package com.example.springporfolio.repository;

import com.example.springporfolio.Modelo.Fotosperfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FotoperfilRepository extends JpaRepository<Fotosperfil, Long> {
}
