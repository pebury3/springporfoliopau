package com.example.springporfolio.repository;

import com.example.springporfolio.Modelo.Lenguajes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LenguajeRepository extends JpaRepository<Lenguajes, Long>{
}
