package com.example.springporfolio.Service;


import com.example.springporfolio.Modelo.Personal;
import com.example.springporfolio.repository.PersonalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonalService {
    @Autowired
    private PersonalRepository personalRepository;

    public ArrayList<Personal> getPersonal()
    {
        return (ArrayList<Personal>) personalRepository.findAll();
    }

}
