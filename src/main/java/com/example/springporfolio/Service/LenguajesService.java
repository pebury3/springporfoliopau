package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Lenguajes;
import com.example.springporfolio.repository.LenguajeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LenguajesService {
    @Autowired
    private LenguajeRepository lenguajeRepository;

    public ArrayList<Lenguajes> getImg_lenguaje() {

       return  (ArrayList<Lenguajes>) lenguajeRepository.findAll();
    }
    public Lenguajes getLenguaById(Long id){
        return lenguajeRepository.findById(id).orElse(null);
    }

}
