package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Services;
import com.example.springporfolio.repository.LenguajeRepository;
import com.example.springporfolio.repository.ServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServicesService {
    @Autowired
    private ServicesRepository servicesRepository;

    public ArrayList<Services> getImg_service() {

        return  (ArrayList<Services>) servicesRepository.findAll();
    }
    public Services getId_service(Long id){
        return servicesRepository.findById(id).orElse(null);
    }

}