package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Fotosperfil;
import com.example.springporfolio.repository.FotoperfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class FotoperfilService {
    @Autowired
    private FotoperfilRepository FotoperfilRepository;

    public ArrayList<Fotosperfil> getImg_Fotosperfil() {

        return  (ArrayList<Fotosperfil>) FotoperfilRepository.findAll();
    }
    public Fotosperfil getId_Fotosperfil(Long id){
        return FotoperfilRepository.findById(id).orElse(null);
    }

}