package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Lenguajes2;
import com.example.springporfolio.repository.Lenguaje2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class Lenguajes2Service {
    @Autowired
    private Lenguaje2Repository lenguaje2Repository;

    public ArrayList<Lenguajes2> getImg_lenguaje() {

        return  (ArrayList<Lenguajes2>) lenguaje2Repository.findAll();
    }
    public Lenguajes2 getLenguaById(Long id){
        return lenguaje2Repository.findById(id).orElse(null);
    }

}