package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Proyectos;
import com.example.springporfolio.repository.ProyectosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ProyectosService {
    @Autowired
    private ProyectosRepository proyectosRepository;

    public ArrayList<Proyectos> getImg_proyecto() {

        return  (ArrayList<Proyectos>) proyectosRepository.findAll();
    }
    public Proyectos getId_proyecto(Long id){
        return proyectosRepository.findById(id).orElse(null);
    }

}
