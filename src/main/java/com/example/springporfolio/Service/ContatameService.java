package com.example.springporfolio.Service;

import com.example.springporfolio.repository.ContactameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.springporfolio.Modelo.Contactame;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatameService {
    @Autowired
    private ContactameRepository contactameRepository;

    public List<Contactame> getCorreo_contatame() {
        return contactameRepository.findAll();
    }

    public Contactame addContacto(Contactame c){
        return contactameRepository.save(c);
    }


}
