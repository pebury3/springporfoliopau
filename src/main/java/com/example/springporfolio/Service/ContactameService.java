package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Contactame;
import com.example.springporfolio.repository.ContactameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ContactameService {
    @Autowired
    private ContactameRepository contactameRepository;
    public Contactame addContactame(Contactame contactame) {
        return contactameRepository.save(contactame);
    }



}