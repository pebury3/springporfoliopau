package com.example.springporfolio.Service;

import com.example.springporfolio.Modelo.Services3;
import com.example.springporfolio.repository.ServicesRepository3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServicesService3 {
    @Autowired
    private ServicesRepository3 servicesRepository3;

    public ArrayList<Services3> getImg_service() {

        return  (ArrayList<Services3>) servicesRepository3.findAll();
    }
    public Services3 getId_service(Long id){
        return servicesRepository3.findById(id).orElse(null);
    }

}