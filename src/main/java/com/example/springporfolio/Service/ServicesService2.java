package com.example.springporfolio.Service;


import com.example.springporfolio.Modelo.Services2;
import com.example.springporfolio.repository.ServicesRepository2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServicesService2 {
    @Autowired
    private ServicesRepository2 servicesRepository2;

    public ArrayList<Services2> getImg_service() {

        return  (ArrayList<Services2>) servicesRepository2.findAll();
    }
    public Services2 getId_service(Long id){
        return servicesRepository2.findById(id).orElse(null);
    }

}