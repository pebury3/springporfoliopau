package com.example.springporfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPorfolioApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringPorfolioApplication.class, args);

    }
}
