package com.example.springporfolio.Modelo;


import jakarta.persistence.*;
@Entity
@Table(name="Proyectos")
public class Proyectos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_proyecto;
    private String nombre_proyecto;
    private String img_proyecto;

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }

    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getImg_proyecto() {
        return img_proyecto;
    }

    public void setImg_proyecto(String img_proyecto) {
        this.img_proyecto = img_proyecto;
    }

    public void setId_proyecto(Long id_proyecto) {
        this.id_proyecto = id_proyecto;
    }

    public Long getId_proyecto() {
        return id_proyecto;
    }
}
