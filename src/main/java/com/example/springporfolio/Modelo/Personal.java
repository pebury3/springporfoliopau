package com.example.springporfolio.Modelo;
import jakarta.persistence.*;
@Entity
@Table(name="Personal")
public class Personal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String nombre;
    private String apellido;
    private String rol;
    private String descripcionrol;
    private String img;
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getRol() {
        return rol;
    }
    public void setRol(String rol) {
        this.rol = rol;
    }
    public String getDescripcionrol() {
        return descripcionrol;
    }
    public void setDescripcionrol(String descripcionrol) {
        this.descripcionrol = descripcionrol;
    }
    public String getImg() {
        return img;
    }
    public void setImg(String img) {
        this.img = img;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
}
