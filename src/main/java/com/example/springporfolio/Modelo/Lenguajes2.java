package com.example.springporfolio.Modelo;


import jakarta.persistence.*;
@Entity
@Table(name="Lenguajes2")
public class Lenguajes2 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_lenguajes;
    private String nombre_lenguaje;
    private String img_lenguaje;

    public String getNombre_lenguaje() {
        return nombre_lenguaje;
    }

    public void setNombre_lenguaje(String nombre_lenguaje) {
        this.nombre_lenguaje = nombre_lenguaje;
    }

    public String getImg_lenguaje() {
        return img_lenguaje;
    }

    public void setImg_lenguaje(String img_lenguaje) {
        this.img_lenguaje = img_lenguaje;
    }

    public void setId_lenguajes(Long idLenguajes2) {
        this.id_lenguajes = idLenguajes2;
    }

    public Long getId_lenguajes() {
        return id_lenguajes;
    }
}
