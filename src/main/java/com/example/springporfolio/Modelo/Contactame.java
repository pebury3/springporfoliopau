package com.example.springporfolio.Modelo;

import jakarta.persistence.*;

@Entity
@Table(name="Contactame")
public class Contactame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_contatame;
    private String correo_contatame;
    private String comentario_contactame;

    public Long getId_contatame() {
        return id_contatame;
    }

    public void setId_contatame(Long id_contatame) {
        this.id_contatame = id_contatame;
    }

    public String getCorreo_contatame() {
        return correo_contatame;
    }

    public void setCorreo_contatame(String correo_contatame) {
        this.correo_contatame = correo_contatame;
    }

    public String getComentario_contactame() {
        return comentario_contactame;
    }

    public void setComentario_contactame(String comentario_contactame) {
        this.comentario_contactame = comentario_contactame;
    }
}
