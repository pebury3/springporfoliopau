package com.example.springporfolio.Modelo;


import jakarta.persistence.*;
@Entity
@Table(name="Services3")
public class Services3 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_service;
    private String nombre_service;
    private String img_service;

    public String getNombre_service() {
        return nombre_service;
    }

    public void setNombre_service(String nombre_service) {
        this.nombre_service = nombre_service;
    }

    public String getImg_service() {
        return img_service;
    }

    public void setImg_service(String img_service) {
        this.img_service = img_service;
    }

    public void setId_service(Long id_service) {
        this.id_service = id_service;
    }

    public Long getId_service() {
        return id_service;
    }
}
