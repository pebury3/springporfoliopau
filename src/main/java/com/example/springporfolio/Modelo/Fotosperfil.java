package com.example.springporfolio.Modelo;


import jakarta.persistence.*;
@Entity
@Table(name="Fotosperfil")
public class Fotosperfil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_Fotosperfil;
    private String nombre_Fotosperfil;
    private String img_Fotosperfil;

    public String getNombre_Fotosperfil() {
        return nombre_Fotosperfil;
    }

    public void setNombre_Fotosperfil(String nombre_Fotosperfil) {
        this.nombre_Fotosperfil = nombre_Fotosperfil;
    }

    public String getImg_Fotosperfil() {
        return img_Fotosperfil;
    }

    public void setImg_Fotosperfil(String img_Fotosperfil) {
        this.img_Fotosperfil = img_Fotosperfil;
    }

    public void setId_Fotosperfil(Long id_Fotosperfil) {
        this.id_Fotosperfil = id_Fotosperfil;
    }

    public Long getId_Fotosperfil() {
        return id_Fotosperfil;
    }
}
